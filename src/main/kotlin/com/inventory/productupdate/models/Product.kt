package com.inventory.productupdate.models

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonIgnore
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "products")
data class Product (
  public var name: String,
  public var price: Integer
) { }